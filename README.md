# Les enfants perdus de la forêt du Gnu

## Le scénario et les commandes
Bienvenue dans 'Les enfants perdus de la forêt du Gnu'

Tu est un moniteur de colo et tu dois t'assurer d'avoir tous les enfants avant d'embarquer dans le bus de retour.

Que dois-tu faire?
- Retourne au campement
- Dans le campement récupère la liste des enfants qui sont sous ta responsabilité
- Sont-ils tous au campement?
- Part à la recherche des enfants perdus
- N'hésite pas à demander aux enfants présents s'ils savent ou sont leurs camarades
- Chaque enfant contient un 'flag', un texte incompréhensible, complete le fichier liste_des_enfants dans le campement
- Quand tu auras trouvé tout le monde,tu pourras enlever les tentes (les supprimer) et monter dans le bus pour terminer le jeu (commande demarrer_bus)

### Rappel des commandes utiles:

cd - entrer dans un dossier

ls - lister les fichiers

rm - supprimer un fichier

pwd - afficher le chemin du dossier actuel

cat - afficher le contenu d'un fichier

grep - rechercher un texte dans un fichier ou dans toute une arborecense (-R)

find - trouver un fichier

ip - pour connaitre l'ip  de ta machine (-c)

curl - faire une requette sur le réseau

nano - modifier un fichier

mkdir - creer un dossier

### Commandes propres au jeu:

faire_parler \$nom_du_fichier => exemple : faire_parler fichier.txt : permet de décoder le texte d'un enfant pour comprendre ce qu'il dit

demarrer_bus => le jeu va vérifier si tous les enfants on ététrouvé, il le sait si on a bien completé le fichier liste_des_enfants

BON JEU A TOUS !

### Tapez "aide" dans le terminal...pour retrouver ce texte dans le jeu

## Fin du jeu
Le jeu se termine quand ce message apparait
```text
Félicitations ! ╰( ͡° ͜ʖ ͡° )つ──☆*:・ﾟ
            
Tu a réussi le chanlenge !

Tu peux aider les autres mais ne leur donne pas la solution ʕっ•ᴥ•ʔっ
```

Pour y arriver il faut trouver tous les enfants et remplir le fichier liste_des_enfants qui est dans le campement.

```
Liste des enfants
- bob:TCdgdrghdtw6AgbCdhaXIgZdrgdgr! <= c'est pas le vrai
- abdoul: <= trouver le flag sur chaque enfant
- noemie:
- alex:
- thiery:
- lydia:
- seraphine:
```

Quand le fichier est rempli avec tous les flags des enfants il suffit de taper `demarrer_bus` pour vérifier que tout est bon.

Si une valeur est mal copiée, le je dira que l'enfant n'est pas trouvé... s'il y a un flag sur cet enfant mais qu'il n'est pas pris en compte c'est qu'il y a une erreur de saisie.

## Déployment
Ajoutez le nom des participants dans le fichier "chasse_aux_tresors.sh", ligne 7 => users=("participant" "autre_participant" "etc")
A la ligne suivante, la variable password contient le mot de passe pour la connection ssh

### Lancer docker
`docker-compose up -d`

### Se connecter au serveur
`ssh participant@ip-du-serveur`

Avec le mot de passe de la variable password

## Suppression
docker-compose down && docker rmi atelier_chasse_aux_tresors_anna atelier_chasse_aux_tresors_python atelier_chasse_aux_tresors_ssh