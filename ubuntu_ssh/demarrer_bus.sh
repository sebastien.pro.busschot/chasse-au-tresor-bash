#!/bin/bash
game_over=true

child_founded(){
    child=$1
    hash=$2
    if ! cat /home/$USER/clairiere/foret/campement/liste_des_enfants | grep $hash &> /dev/null
    then
        echo "$child n'a pas été retrouvé"
        game_over=false
    fi
}

test_win(){
    child_founded bob TCdlYXUgw6AgbCdhaXIgZnJvaWRlCg==
    child_founded ricky VHUgbidhdXJhaXMgcGFzIHZ1IEx5ZGlhPyBFbGxlIGV0YWl0IGF2ZWMgbW9pLiBKZWNyb2lzIHF1J2xsZSBzJ2VzdCBwZXJkdWUgZGFucyBsYSBmb3LDqnQgcHJvZm9uZGUuLi4K
    child_founded abdoul Q2V0dGUgY2F2ZXJuZSBlc3QgbWFnbmlmaXF1ZQo=
    child_founded noemie SidhaSB2dSBMeWRpYSBpbCB5IGEgdW5lIGhldXJlLiBFbGxlIGV0YWlzIGRhbnMgc2EgdGVudGUuCg==
    child_founded alex QWJkb3VsIGVzdGFpdCBoeXBlciBlbWJhbGzDqSBwYXIgdW5lIGZvbnRhaW5lIGF1IG5vcmQuLi4K
    child_founded thiery QXZlYyByaWNreSBvbiBzJ2VzdCBkaXNwdXTDqSBkYW5zIHNhIHRlbnRlCg==
    child_founded lydia SmUgbSdldGFpcyBwZXJkdWUuIE1lcmNpIGRlIG0nYXZvaXIgcmV0cm91dsOpZQo=
    child_founded seraphine aGloaWhpLiBKJ2V0YWlzIGJpZW4gY2FjaMOpZSA6RAo=
    if $game_over 
    then
        hash=TGUgY291cmFudCBldGFpdCB0csOocyBmb3J0LCB0dSBsJ2VzdCB0b3V0IGF1dGFudCBkZSBtJ2F2b2lyIHJldHJvdXbDqQo=
        if ! cat /home/$USER/clairiere/foret/campement/liste_des_enfants | grep $hash &> /dev/null
        then
            echo "Tu les tous trouvés !"
            sleep 2
            echo "Mais ou est Anna? Elle surfait sur le fleuve Dunet... aurais-t-elle dérivé sur une autre ip?"
            echo "- anna:" >> /home/$USER/clairiere/foret/campement/liste_des_enfants
            game_over=false
        fi
    fi
}

tent_umounted(){
    if ls /home/$USER/clairiere/foret/campement/tente* &> /dev/null
    then
        echo "Il reste des tentes au campement"
        exit 1
    fi
}

finish(){
    if $game_over
    then
        clear
        echo """
            Félicitations ! ╰( ͡° ͜ʖ ͡° )つ──☆*:・ﾟ
            
            Tu a réussi le chanlenge !
            
            Tu peux aider les autres mais ne leur donne pas la solution ʕっ•ᴥ•ʔっ
        """
    fi
}


tent_umounted
test_win
finish