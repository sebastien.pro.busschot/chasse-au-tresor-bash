#!/bin/bash

help(){
    echo "usage: faire_parler \$nom_du_fichier"
}

if [[ $# -eq 0 ]]
then
    help
    exit 1
fi

if ls $1 &>/dev/null
then
    if [[ -f  $1 ]]
    then
        cat $1 | base64 -d
        exit 0
    else
        echo "$1 n'est pas un fichier'"
    fi
else
    echo "$1 n'existe pas"
fi

exit 1