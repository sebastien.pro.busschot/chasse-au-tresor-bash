#!/bin/bash

echo -e """
Bienvenue dans 'Les enfants perdus de la forêt du Gnu'

Tu est un moniteur de colo et tu dois t'assurer d'avoir tous les enfants avant d'embarquer dans le bus de retour.

Que dois-tu faire?
- Retourne au campement
- Dans le campement récupère la liste des enfants qui sont sous ta responsabilité
- Sont-ils tous au campement?
- Part à la recherche des enfants perdus
- N'hésite pas à demander aux enfants présents s'ils savent ou sont leurs camarades
- Chaque enfant contient un 'flag', un texte incompréhensible, complete le fichier liste_des_enfants dans le campement
- Quand tu auras trouvé tout le monde,tu pourras enlever les tentes (les supprimer) et monter dans le bus pour terminer le jeu (commande demarrer_bus)

Rappel des commandes utiles:
cd - entrer dans un dossier
ls - lister les fichiers
rm - supprimer un fichier
pwd - afficher le chemin du dossier actuel
cat - afficher le contenu d'un fichier
grep - rechercher un texte dans un fichier ou dans toute une arborecense (-R)
find - trouver un fichier
ip - pour connaitre l'ip  de ta machine (-c)
curl - faire une requette sur le réseau
nano - modifier un fichier
mkdir - creer un dossier

Commandes propres au jeu:
faire_parler \$nom_du_fichier => exemple : faire_parler fichier.txt : permet de décoder le texte d'un enfant pour comprendre ce qu'il dit
demarrer_bus => le jeu va vérifier si tous les enfants on ététrouvé, il le sait si on a bien completé le fichier liste_des_enfants

BON JEU A TOUS !
"""