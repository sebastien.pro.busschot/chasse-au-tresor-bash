#!/bin/bash

############################################################################# Les enfants perdus de la forêt du Gnu ######################################################

/etc/init.d/ssh start

users=("user1" "user2")
password="password"

user=""

set_user(){
    user=$1
    pass=$2
    useradd --create-home --shell /bin/bash -m -p $pass $user
    setGame
    chown -R $user:$user /home/$user
}

set_users(){
    pass=$(perl -e 'print crypt($ARGV[0], "password")' $password)

    counter=1 
    for user in ${users[*]}
    do
        set_user $user $pass &
    done
}

clues(){
    echo -e "Noemie, avec Lydia on a trouvé une cabanne a coté de la forêt profonde" > /home/$user/clairiere/foret/campement/tente_ricky/indice
    echo -e "Cocou je suis partie ave #e9e [Le document est illisible]" > /home/$user/clairiere/foret/campement/tente_lydia/indice
    echo -e "Liste des enfants\n- bob:\n- ricky:\n- abdoul:\n- noemie:\n- alex:\n- thiery:\n- lydia:\n- seraphine:" > /home/$user/clairiere/foret/campement/liste_des_enfants
    echo -e "Lydia est ici" > /home/$user/clairiere/foret/campement/sentier_sud/foret_profonde/lieu_25/mot_de_lydia
}

# Liste des enfants
# - bob:TCdlYXUgw6AgbCdhaXIgZnJvaWRlCg==
# - ricky:VHUgbidhdXJhaXMgcGFzIHZ1IEx5ZGlhPyBFbGxlIGV0YWl0IGF2ZWMgbW9pLiBKZWNyb2lzIHF1J2xsZSBzJ2VzdCBwZXJkdWUgZGFucyBsYSBmb3LDqnQgcHJvZm9uZGUuLi4K
# - abdoul:Q2V0dGUgY2F2ZXJuZSBlc3QgbWFnbmlmaXF1ZQo=
# - noemie:SidhaSB2dSBMeWRpYSBpbCB5IGEgdW5lIGhldXJlLiBFbGxlIGV0YWlzIGRhbnMgc2EgdGVudGUuCg==
# - alex:QWJkb3VsIGVzdGFpdCBoeXBlciBlbWJhbGzDqSBwYXIgdW5lIGZvbnRhaW5lIGF1IG5vcmQuLi4K
# - thiery:QXZlYyByaWNreSBvbiBzJ2VzdCBkaXNwdXTDqSBkYW5zIHNhIHRlbnRlCg==
# - lydia:SmUgbSdldGFpcyBwZXJkdWUuIE1lcmNpIGRlIG0nYXZvaXIgcmV0cm91dsOpZQo=
# - seraphine:aGloaWhpLiBKJ2V0YWlzIGJpZW4gY2FjaMOpZSA6RAo=
# - anna:TGUgY291cmFudCBldGFpdCB0csOocyBmb3J0LCB0dSBsJ2VzdCB0b3V0IGF1dGFudCBkZSBtJ2F2b2lyIHJldHJvdXbDqQo=

childrens(){
    echo -e "L'eau à l'air froide" | base64 > /home/$user/clairiere/abors_du_lac/bob
    echo -e "Tu n'aurais pas vu Lydia? Elle etait avec moi. Jecrois qu'lle s'est perdue dans la forêt profonde..." | base64 > /home/$user/clairiere/foret/campement/sentier_sud/cabanne/ricky
    echo -e "Cette caverne est magnifique" | base64 > /home/$user/clairiere/foret/campement/sentier_nord/.entree_dissimulee/caverne_des_777_tresors/abdoul
    echo -e "J'ai vu Lydia il y a une heure. Elle etais dans sa tente." | base64 > /home/$user/clairiere/foret/campement/noemie
    echo -e "Abdoul estait hyper emballé par une fontaine au nord..." | base64 > /home/$user/clairiere/foret/campement/alex
    echo -e "Avec ricky on s'est disputé dans sa tente" | base64 > /home/$user/clairiere/foret/campement/thiery
    echo -e "Je m'etais perdue. Merci de m'avoir retrouvée" | base64 > /home/$user/clairiere/foret/campement/sentier_sud/foret_profonde/lieu_25/lydia
    echo -e "hihihi. J'etais bien cachée :D" | base64 > /tmp/cachette/seraphine
}

create_forest(){
    root_fs=$1

    counter=1 
    echo -e "Foret profonde de $user en cours de fabrication..."
    until [ $counter -gt 3 ]
    do
        for dir in $(find "$root_fs" -type d)
        do
            dir_count=$(( $RANDOM % 10 + 5 ))
            counter_b=1 
            until [ $counter_b -gt $dir_count ]
            do
                mkdir "${dir}/lieu_${counter}${counter_b}" 2> /dev/null
                ((counter_b++))
            done
        done
        ((counter++))
    done
    echo -e "Foret profonde de $user terminée"
}

place_item(){
    name="$1"
    count=$2
    dir=$3
    
    counter=1 
    until [ $counter -gt $count ]
    do
        touch "${dir}/${name}${counter}"
        ((counter++))
    done
}

fs_generate(){
    echo "Génération de la \"map\" de $user..."

    mkdir -p /home/$user/clairiere/{abors_du_lac,foret,chemin}
    mkdir -p /home/$user/clairiere/foret/{campement,sentier,ruisseau}
    mkdir -p /home/$user/clairiere/foret/campement/{chemin,sentier_nord,sentier_sud}
    mkdir -p /home/$user/clairiere/foret/campement/sentier_nord/{source_open,.entree_dissimulee,chemin}
    mkdir -p /home/$user/clairiere/foret/campement/sentier_sud/{chemin,foret_profonde}
    mkdir -p /tmp/cachette
    
    create_forest "/home/$user/clairiere/foret/campement/sentier_sud/foret_profonde"

    for dir in $(find /home/$user -type d)
    do
        number_of_tree=$(( $RANDOM % 10 + 0 ))
        number_of_grass=$(( $RANDOM % 30 + 0 ))
        number_of_rock=$(( $RANDOM % 5 + 0 ))
        place_item "arbre" $number_of_tree $dir
        place_item "herbe" $number_of_grass $dir
        place_item "rocher" $number_of_rock $dir
    done

    echo "Pikachu !" > /home/$user/clairiere/.pikachu

    mkdir -p /home/$user/clairiere/foret/campement/sentier_nord/.entree_dissimulee/caverne_des_777_tresors
    mkdir -p /home/$user/clairiere/foret/campement/sentier_sud/cabanne
    mkdir -p /home/$user/clairiere/foret/campement/{tente_bob,tente_ricky,tente_abdoul,tente_noemie,tente_alex,tente_thiery,tente_lydia,tente_seraphine,tente_anna}

    echo "\"map\" de $user terminée"
}

chmodx(){
    chmod +x /usr/local/bin/demarrer_bus
    chmod +x /usr/local/bin/aide
    chmod +x /usr/local/bin/faire_parler
}

setGame(){
    fs_generate
    childrens
    clues    
}

main(){
    chmodx
    set_users
    tail -f /dev/null
}

main